# Docker
### Introduction

2019-03-21 - PCH CIOB Tech Talks

---

## Plan

- What is a Container?
- Docker images - Dockerfiles
- Docker commands
- Docker volumes
- Docker Compose
- Docker Swarm
- Examples (demo)

---

## Containers

- Open Standard (runc & containerd)
- Packaging for software and dependencies
- Isolate apps from each other
- Share the same OS kernel
- Works with Linux and Windows Server

--

<img src="https://www.docker.com/sites/default/files/d8/styles/large/public/2018-11/container-what-is-container.png" alt="containers" width="750"/>

--

## Containers & VMs

<img src="https://www.docker.com/sites/default/files/d8/2018-11/docker-containerized-and-vm-transparent-bg.png" alt="containers" width="1080"/>

---

## Why containers

- Abstraction that packages code and dependencies
- Run and manage apps side-by-side in isolation
- Multiple containers can share OS resources
- Take up less space than VMs
- More efficient use of infrastructure
- Configuration and Infrastructure as code
- Eliminate “works on my machine” problems
- Continuous integration and delivery pipelines
- Great tool for developers and sysadmins

---

## Security
- **Namespaces** provide isolation
  - processes running within a container cannot see, and even less affect, processes running in another container, or in the host system
- Each container also gets **its own network stack**
- Containers are secure; especially if you take care of running your processes inside the containers as **non-privileged users**

https://docs.docker.com/engine/security/security/

---

## Docker images

- Configuration as code
- Lightweight, stand-alone package
- Everything needed to run a piece of software

Docker Hub - https://hub.docker.com/explore/

--

### Dockerfiles

- Define images for containers
- Everything on Docker Hub has Dockerfile
  - All Versions
- You can write your own!

--

### Commands

- `docker pull` # get image from hub
- `docker build` # build image from Dockerfile
- `docker images` # show images
- `docker rmi` # remove image(s)

---

## Docker Containers

Runtime instance of an image

What the image becomes in memory when actually executed

--

### Commands

- `docker ps` # show containers
- `docker run` # start new container from image
- `docker start` or `stop` # start or stop container
- `docker logs` # show logs for a running container
- `docker exec` # run command on running container
- `docker --help`

---

## Docker Volumes

If you delete a container all data is gone

Persistant data on host using volumes

- Mapped volume
- Named volume

---

## Docker Compose

- Configuration as code
- Put it all together
- Automate docker commands
- docker-compose.yml

`docker-compose up`

---

## Docker Swarm

- Service = One or more containers
  - Acting as one service
- Can across accross networks
- Fault tolerance
- Load balancing

---

## Container Orchestration

- Docker Swarm
- Kubernetes
  - OpenShift
  - Rancher
- Amazon EC2 Container Service
- Google Kubernetes Engine
- Azure Container Service

---

## Links

- https://www.docker.com/resources/what-container
- https://www.docker.com/resources
- https://www.cncf.io/

### Docker Playground

- https://labs.play-with-docker.com/
- https://training.play-with-docker.com/

---

_blank

---

## Examples

- Debian
- Hacker Slides
- NodeJS
- nginx
- Jekyll
- Drupal + MariaDB
- Swarm

---

### Debian

`docker run -it --rm debian bash`

---

### Hacker Slides

docker run

-p 8080:8080

-v $(pwd):/app/slides -it --rm

msoedov/hacker-slides

--

`docker run -p 8080:8080 -v $(pwd):/app/slides -it --rm msoedov/hacker-slides`

---

### NodeJS

`docker run -v $(pwd):/usr/node/app -w /usr/node/app -it --rm node npm install`

---

### Nginx

`docker run -p 80:80 -v $(pwd):/usr/share/nginx/html -it --rm nginx`

---

### Jekyll

`docker run -p 4000:4000 -v $(pwd):/srv/jekyll -it --rm jekyll/jekyll jekyll serve`

---

### Drupal

#### Mysql

`docker run --name drupal-db -e MYSQL_DATABASE=drupal -e MYSQL_USER=drupal -e MYSQL_PASSWORD=password -e MYSQL_ROOT_PASSWORD=root_password -d mysql:5.7`

--

#### Drupal

`docker run -p 8000:80 --link drupal-db:mysql -d drupal`

---

### Docker Swarm

`docker swarm init --advertise-addr 192.168.0.1`

--

`docker service create -p 80:80 httpd`

--

- `docker node ls`
- `docker service ps SRVC_NAME`
- `docker service scale SRVC_NAME=5`
- `docker ps`

---


Fin!
