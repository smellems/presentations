# Présentations

To view presentation using HackerSlides and Docker

- Clone repo
- Run `docker run -p 8080:8080 -v $(pwd):/app/slides -it --rm msoedov/hacker-slides`
- Goto http://localhost:8080/stash/
