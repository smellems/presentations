# Directive on the Management of IT

### Guidance on Open Source Software

2019-03-27 - PCH ARB

---

## Plan

- Updates to IT Policy and Directive
  - Mandatory EA Procedures
- Draft Guidance
  - Using Open Source Software
  - Publishing Open Source Code
  - Contributing to Open Source Communities

---

## Updated IT Directive

2018-12-04

Mandatory Procedures for EA Assessment

- Based on TOGAF (BIATS)
- Digital Standards
- Digital Architecture Standards

--

### Architecture

- **B**usiness
- **I**nformation
- **A**pplication
- **T**echnology
- **S**ecurity and Privacy

---

## Business Architecture

https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=15249#claC.2.3.1

- Share, collaborate, align
- Users' needs
- Accountability

--

### Align to the GC Business Capability model

Identify capabilities that are common to the GC enterprise and can be shared and reused

Model business processes using Business Process Management Notation (BPMN) to identify common enterprise processes 

--

### Design for Users First

#### Deliver with Multidisciplinary Teams

Focus on the needs of users, using agile, iterative, and user-centred methods

Work across the entire application lifecycle, from development and testing to deployment and operations

Encourage and adopt Test Driven Development (TDD) to improve the trust between Business and IT

--

### Design Systems to be Measurable and Accountable

Publish performance expectations for each IT service

Establish business and IT metrics to enable business outcomes

Apply oversight and lifecycle management to digital investments through governance

---

## Information Architecture

https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=15249#claC.2.4

- Maximizes use and availability
- Standards and ethical practices

--

### Data Collection

Ensure data is collected in a manner that maximizes use and availability of data

Ensure data collected aligns to existing enterprise and international standards

Where enterprise or international standards don't exist, develop Standards in the open with key subject matter experts

--

### Data Management

Demonstrate alignment with enterprise and departmental data governance and strategies

Ensure accountability for data roles and responsibilities

Design to maximize data use and availability

--

### Data Storage

Ensure data is stored in a secure manner in accordance with the National Cyber Security Strategy, and the Privacy Act

Follow existing retention and disposition schedules

Ensure data is stored in a way to facilitate easy data discoverability, accessibility, and interoperability

--

### Data Sharing

Data should be shared openly by default as per the Directive on Open Government

Ensure government-held data can be combined with data from other sources enabling interoperability and interpretability through for internal and external use

Reduce the collection of redundant data

Reuse existing data where possible

Encourage data sharing and collaboration

---

## Application Architecture

https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=15249#claC.2.8

- Open standards and open source software
- Reuse
- Interoperability

--

### Use Open Standards and Solutions by Default

Where possible, use open standards and open source software first

If an open source option is not available or does not meet user needs, favour platform-agnostic COTS over proprietary COTS, avoiding technology dependency, allowing for substitutability and interoperability

All source code must be released under an appropriate open source software license

--

### Maximize Reuse

Leverage and reuse existing solutions, components, and processes

Select enterprise and cluster solutions over department-specific solutions

Inform the GC EARB about departmental investments and innovations

Share code publicly when appropriate, and when not, share within the Government of Canada

--

### Enable Interoperability

Expose all functionality as services (API)

Use microservices built around business capabilities. Scope each service to a single purpose

Run applications in containers

Leverage the GC Digital Exchange Platform for components such as the API Store, Messaging, and the GC Service Bus

---

## Technology Architecture

https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=15249#claC.2.11

- Cloud
- Performance, availability, and scalability

--

### Use Cloud first

Enforce this order of preference: Software as a Service (SaaS) first, then Platform as a Service (PaaS), and lastly Infrastructure as a Service (IaaS)

Enforce this order of preference: Public cloud first, then Hybrid cloud, then Private cloud, and lastly non-cloud (on-premises) solutions

Design for cloud mobility and develop an exit strategy to avoid vendor lock-in

--

### Design for Performance, Availability, and Scalability

Design for resiliency

Ensure response times meet user needs for availability

Support zero-downtime deployments for planned and unplanned maintenance

Use distributed architectures, assume failure will happen, handle errors gracefully, and monitor actively

---

## Security Architecture and Privacy

https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=15249#claC.2.11

- Security and privacy
- Across all architectural layers

--

### Design for Security and Privacy

Implement security across all architectural layers

Categorize data properly to determine appropriate safeguards

Perform a privacy impact assessment (PIA) and mitigate all privacy risks when personal information is involved

Balance user and business needs with proportionate security measures and adequate privacy protections.

---

## General Themes

- Users, Agile, DevOps
- Collaboration, Sharing, Reuse
- Accountability, Security, Privacy
- Interoperability
- Open data, standards, software, ..
- Cloud
- Avoid lock-in

https://open.canada.ca/en/blog/open-foundation-digital-government

---

## Firsts

1. Users
1. Open Standards
1. Open Source Software
1. Cloud

---

_blank

---

## Open First Whitepaper

https://github.com/canada-ca/Open_First_Whitepaper

- Inform EARB and TBS direction
- Unpack "open" for the GC
  - Open Standards
  - Open Source Software Use
  - Open Source Software Contribution
  - Open Markets
  - Open Culture (Organization)

--

### What happened?

- In first 2 months
  - 100 Stars
  - 200 changes by 20 contribtors
  - 40 issues with active discussions
- Open Ressource Exchange
  - https://canada-ca.github.io/ore-ero/ 

--

### Business Needs

> Offer the best digital services possible

- Working in the open by default
- Interoperability between systems
- Independence and substitutability
- Avoiding application adherence
- Attract digital talent and increase job satisfaction
- Embracing leading practices and the latest technologies
- Provide public benefits to the public-at-large

---

_blank

---

## Guidance on Open Source Software

### Draft

https://github.com/canada-ca/open-source-logiciel-libre/tree/master/en/guides

- Using open source software (OSS)
- Publishing open source code
- Contributing to open source communities

---

## Using Open Source Software

> Open source software must be selected first on the basis of its additional inherent flexibility and interoperability benefits, when there is no significant functionality or cost difference with closed-source solutions.

https://github.com/canada-ca/open-source-logiciel-libre/blob/master/en/guides/using-open-source-software.md

---

### Actively and Fairly Consider OSS

- Open source software is not completely free
- Take into account the total cost of ownership (TCO)
  - including exit and transition costs

--

#### Evaluation

Criteria to be assessed when evaluating OSS:

- User Community
- Developer Community
- History of releases and patches
- Documentation
- Availability of support

--

#### Open Source Software Licenses

All software licensed under an

- [Open Source Initiative approved license](https://opensource.org/licenses)
- [Free Software Foundation free software licence](https://www.gnu.org/licenses/license-list.html)

can be used by GC.

--

#### Evaluate Support Options

- Internal support (community)
- Support services (professional & community)

--

#### Other Consideration

If OSS meets most user needs but would require additional investment to develop missing functionality.

This investment must be considered by weighing the TCO against those of other candidate solutions.


---

### Use OSS Without Modification

> Using open source software without modification does not require that code be shared back.

Configuration of software, through configuration files, is not considered modifications.

---

### Use OSS With Modifications

> Using open source software with modifications is not generally* considered distribution and does not require that code be shared back.

Modifications made to open source software should still be shared with the community to help ensure sustainability of the solution.

*Exception for strong reciprocal licences.

--

#### Strong Reciprocal Licences

Software accessed through a network (like the Internet) is considered distribution and the modified source code must be made available to users.

- AGPL 3.0
- EUPL

---

### Don't Fork Open Source Software

> Where possible, use open source software without modification.

Use configuration and customize the software with modules, plugins or extensions.

If a fork is created, be aware it can make future updates and security patches hard to implement.

To make changes to OSS, engage with the community and submit changes upstream to ensure that they are supported by future updates.

---

## Publishing Open Source Code

> All source code, whether developed in-house by GC or through procurement contracts, must be published under an appropriate open source software licence. When unable to publish source code publicly, it must be shared within the Government of Canada.

https://github.com/canada-ca/open-source-logiciel-libre/blob/master/en/guides/publishing-open-source-code.md

---

### Seek Approvals

Similar to open data or information covered by the [Directive on Open Government](https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=28108), the publication of source code under an open source software licence, requires appropriate department or agency approvals.

---

### Obtain Rights to Custom Code in Contracts 

The ISED [Policy on Title to Intellectual Property Arising Under Crown Procurement Contracts](https://www.ic.gc.ca/eic/site/068.nsf/eng/00005.html) provides that the contractor is to own the rights to foreground intellectual property (IP) created as a result of a Crown procurement contract.

But when the Crown's intended use of the IP can be met through licence arrangements, it has the opportunity to seek the needed licence(s) whether broad or narrow.

---

### Select an OSS Licence

When the project is part of a larger open source software community, like plugins, modules, extensions or derivative works of existing open source software, use the license which is usually used by the community.

See Contributing.

--

#### GC Recommended permissive licences

- MIT
- Apache 2.0

--

#### GC Recommended reciprocal licences

- GPL 3.0
- LGPL 3.0
- AGPL 3.0

---

### Select Source Code Repository

#### Recommended public source code repositories are

- [GitLab](https://gitlab.com/)
- [GitHub](https://github.com/)
- [framagit](https://framagit.org/)
- [Bitbucket](https://bitbucket.org/)
- [GCcode](https://gccode.ssc-spc.gc.ca/)
  - Internal to GC only

---

### Work in the Open

#### Release Early, Release Often

Source code should be released as early as possible in the project's life cycle to avoid the overhead of publishing source code late in the process.

The public source code repository should be the single source of truth where developers are working.

--

#### Community

Building a welcoming community can influence your project's future and reputation.

Provide a positive experience for contributors and make it easy so they keep coming back.

Respond to questions, bugs and merge requests.

---

## Contributing to OSS Communities

> All source code modifications made to open source software, whether in-house by GC or through procurement contracts, must be shared back with the open source software community. The GC must be an active contributor to open source software it's using or where there is benefit to Canadians.

https://github.com/canada-ca/open-source-logiciel-libre/blob/master/en/guides/contributing-to-open-source-software.md

---

### There Are Many Ways to Contribute

- Code
- Bug reports and feature requests.
- Improving documentation or adding summaries of longer threads
- Attending open source community events
- Become involved in organizing or sponsoring these events

---

### Verify Open Source Software Licence

GC can contribute code to all software licensed under an

- [Open Source Initiative approved license](https://opensource.org/licenses)
- [Free Software Foundation free software licence](https://www.gnu.org/licenses/license-list.html).

---

### Verify Contributing Process and Policies

- Contributor Licence Agreement
- Developer's Certificate of Origin
- Other process..

Before going forward with submitting a contribution, employees should properly understand the project contribution processes and policies.

---

## Conclusion

- Publish open source code
  - Easy
  - Open Government
  - Open by default
- Use open source software
  - Harder
  - Procurement
  - Culture
  - Change

---

Fin!
