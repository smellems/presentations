# Open Resource Exchange

COGWG - 2019-04-17

---

## What is it?

> Code.gov for Canada

- Search, explore and find contact info
  - Open Source Code and Projects
  - Open Source Software used
  - Open Standards adopted

https://canada-ca.github.io/ore-ero/

--

### History

- Open First Whitepaper
- List of GC open source code
- All Canadian public administrations
- Open development (ongoing)

--

### Open Source Software

- GC Strategic Plan for IM and IT 2017-2021
- Digital Standards and Architectural Standards
- Directive on the Management of IT
- Digital Operations Strategic Plan: 2018-2022

--

### Open Government

2018-2020 National Action Plan on Open Government

> A registry of open source code and open source software is established to provide consolidated access to government open source resources 

---

## Objectives

> Maximize collaboration and sharing

- Open source code published
- Open source software used
- Open standards adopted

**Contact Information!**

---

## Open Data

- Data managed on Github in YAML files
- Working on form to make contributing easy
- JSON dataset available for Open Gov Portal

---

## Open Development

https://github.com/canada-ca/ore-ero

- On Github - MIT License
- Add your data!
- Contribute to development
  - Code
  - Post issues
  - Join discussion

--

### Agile and Ongoing

https://github.com/canada-ca/ore-ero/projects/1

- Kanban
- Weekly Scrum

**Join us!**

---

## Municipal Innovation Pilot Project

### MIPP

Collectively invest resources in open source projects

Increase the investment in and sharing of OSS between public administrations in Canada

---

## Contact us

https://message.gccollab.ca/

#### Development

Sébastien Lemay (PCH)
</br>sebastien.lemay@canada.ca

#### Governance - MIPP

Derek Alton (TBS)
</br>derek.alton@tbs-sct.gc.ca

---

_fin
