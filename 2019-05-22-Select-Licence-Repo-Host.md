# Publishing Open Source Code

## Selecting Licence and Host

PCH ARB - 2019-05-22

---

## Plan

- Why?
- Best Practices
- For Endorsment (encan-auction)
  - Select a licence
  - Select a host (SaaS)
- Working in the open

---

## Why?

- Directive on Open Government
- PCH Mandate Letters: "open by default"
- Directive on Management on IT - EA Procedures
- Digital Standards

https://github.com/canada-ca/Open_First_Whitepaper

--

### Working in the open will help others

- Benefit from your work and build on it
- Learn from your experiences
- Identify parts of your code for reuse
- Make suggestions to improve code or security
- Contribute ideas as the project is in progress

--

### Working in the open will encourage

- Better documentation
- Cleaner and well-structured code
- Processes that will allow you to continuously test and deploy changes
- Clarity around data that needs to remain protected and how that's achieved

---

## Best Practices

[Open First Whitepaper - Best Practices](https://github.com/canada-ca/Open_First_Whitepaper/blob/master/en/4_Open_Source_Software_Contribution.md#best-practices-for-releasing-open-source-software)

[Guidance for Publishing Open Source Code](https://github.com/canada-ca/open-source-logiciel-libre/blob/master/en/guides/publishing-open-source-code.md)

---

## encan-auction

Custom PHP/MySQL application built by PCH for GCWCC.

The code will be moved to a public repository

https://gccode.ssc-spc.gc.ca/PCH/encan-auction

--

### Open Code and Development

Going forward, all development related activities for the project should be done in the open.

- Code changes
- Bugs and feature requests
- Discussions
- Roadmap
- Documentation

No other versions in internal systems (SVN, Tracker, ..).

---

## Where to Host?

GitLab, GitHub, Bitbucket, Framagit, Sourceforge, Launchpad, Savannah..

--

### General Features

- Git
- Issue tracker
- Project management
- Wiki
- Website
- Integrations (CI/CD)

https://en.wikipedia.org/wiki/Comparison_of_source-code-hosting_facilities

--

### Github or Gitlab

Github has the most users and projects

Gitlab is also popular and open source software

--

### Recomendation

## Gitlab

- Multilingual
- Free private repos
- More CI/CD options
- Same software as GCcode

---

## What Licence to Use?

### Permissive or Reciprocal?

--

### Distribution of Derivative works

**Permissive**: Can apply any licence

**Reciprocal**: Must apply same licence

--

If you are distributing a modified version of OSS that uses a reciprocal licence, you MUST make your source code available under the same (or a compatible) reciprocal licence as the original code.

--

### Permissive

**MIT**: Short and simple

**Apache 2.0**:  Provides a grant of patents

--

### Reciprocal

**GPL 3.0**: Base reciprocal licence

**AGPL 3.0**: Hosting on a network is distribution

--

### Recomendation

MIT or AGPL 3.0

---

## For ARB Endorsment

### Hosting

Gitlab

### Licence

MIT

---

## Next Steps

- ARB (today)
- Open Information
- DSO

---

## Other Notes

--

### DevOps

- Code and Configuration
- Containers
- Continuous Integration and Deployments
- Cloud

Version control everything!

--

<img src="https://raw.githubusercontent.com/weaveworks/flux/master/site/images/deployment-pipeline.png"/>

--

### Git

> All new projects should use Git

- Industry standard for version control system
- Used on Github, Gitlab, .. and GCcode

--

### Work in the Open

#### Release Early, Release Often

> All new projects should be open by default

---

_fin
